# Load the Pandas libraries with alias 'pd' 
import pandas as pd 
# Read data from file 'production_data_sample.csv' 
# (in the same directory that your python process is based)
# Control delimiters, rows, column names with read_csv (see later) 
data = pd.read_csv("../data/production_data_sample.csv", sep=';') 
# Preview the first 5 lines of the loaded data 
print(data.head())